<?php

namespace App\Http\Controllers\Movie;

use App\Models\Movie;
use App\Mail\MovieMail;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;

class MovieController extends Controller
{
    public function movieIndex(){
        $movies =Movie::all();
        return view('movies.index', compact('movies'));
      }
    
      public function movieAdd(){
        $movieCategory =DB::table('movie_category')->where('is_active', true)->get();
        return view('movies.add', compact('movieCategory'));
      }
    
    
      public function movieStore(Request $request){
        $validated = $request->validate([
          'title' => 'required',
          'description' => 'required',
          'release_date' => 'required',          
          'is_active' => 'required',
          'featured_image' => 'required',

        ]);
    
        $user = auth()->user()->id;
    
        $movies = new Movie;
        $movies->users_id = $user;
        $movies->movie_category_id = $request->movie_category_id;
        $movies->title = $request->title;
        $movies->description = $request->description;
        $movies->release_date = $request->release_date;
        $movies->is_active = $request->is_active;

        if($request->hasfile('featured_image'))
        {
            $file = $request->file('featured_image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('uploads',$filename);
            $movies->featured_image = $filename;

        }

        $movies->save();
    
        return back()->with('success','Movie Added Successfully');
      }
    
      public function movieEdit($id){
        $movies =Movie::findorFail($id);
        $movieCategory =DB::table('movie_category')->where('is_active', true)->get();
        return view('movies.edit', compact('movies','movieCategory'));
      }
    
      public function movieUpdate(Request $request, $id){
        $validated = $request->validate([
          'title' => 'required',
          'description' => 'required',
          'release_date' => 'required',
        ]);
    
        $user = auth()->user()->id;
        $movies = Movie::find($id);
        $movies->users_id = $user;
        $movies->movie_category_id = $request->input('movie_category_id');
        $movies->title = $request->input('title');
        $movies->description = $request->input('description');
        $movies->release_date = $request->input('release_date');

        if($request->hasfile('featured_image'))
        {
           $destinations = 'uploads/'.$movies->featured_image;
           if(File::exists($destinations))
           {
               File::delete($destinations);
           }

            $file = $request->file('featured_image');
            $extension = $file->getClientOriginalExtension();

            $filename = time().'.'.$extension;
            $file->move('uploads',$filename);
            $movies->featured_image = $filename;

        }
    
        $movies->save();
        return back()->with('success','Movie Edited Successfully'); 
      }
    
      public function changeStatus(Request $request){
        $movie = Movie::find($request->id);
        $movie->is_active = ($request->is_active) ? true :  false;
        $movie->save();
    
        return back()->with('success','Status Changed Successfully'); 
      }
    
      public function destroy($id){
        $movie = Movie::findorFail($id);
        $movie->delete();
    
        return back()->with('success','Movie Deleted Successfully'); 
      }


      // favourite movie 
       public function movieSubmit(Request $request){
            // dd($request->all());
              $user_id = auth()->user()->id;
        
            DB::table('favourite_movie')->insert([
                       'user_id' => $user_id,
                       'movie_id' => $request->id,
                   ]);

                   $user =  Auth::user()->name;  
                   
                   $movie = Movie::find($request->id);

                   Mail::to(Auth::user()->email)->send(new MovieMail($movie, $user));

                   return response()->json(['status' => 1, 'msg' => 'Movie Added To Favourite']);
      
            }

            // favourite movie list
              public function favouriteMovie(){
                 $favourite_movies = DB::table('favourite_movie')
                 ->join('movies', 'movies.id', 'favourite_movie.movie_id')
                 ->where('user_id', '=', auth()->user()->id)
                 ->get();

                  $total =DB::table('favourite_movie')->where('user_id', '=', auth()->user()->id)->get()->count();

                 return view('user.favourite_movie', compact('favourite_movies','total'));
              }

             public function userProfile(){
               return view('user.profile');
             }


            // userlist 
               public function userList(){
                $users =DB::table('users')->where('role_as', '=', '0')->get();
                return view('admin.userlist',compact('users'));
              }
          
              // user's favourite movie
               public function favMovie($id){
                       $fav_movies = DB::table('favourite_movie')
                       ->join('movies','movies.id','favourite_movie.movie_id')
                       ->join('users','users.id','favourite_movie.user_id')
                       ->where('favourite_movie.user_id', $id)
                       ->get();

                      //  dd($fav_movies);
                        return view('admin.fav_movie',compact('fav_movies'));
                    }


      // search
    public function search(Request $request){
      $fav_movies= DB::table('movies')
      ->join('favourite_movie', 'favourite_movie.movie_id', 'movies.id')
      ->whereBetween('release_date', array($request->fromDate, $request->toDate))
      ->get();

      return view('admin.fav_movie', compact('fav_movies'));

    }

}
