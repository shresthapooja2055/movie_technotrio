<?php

namespace App\Http\Controllers\Home;

use App\Models\Movie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    
    public function index(){
        $movies =Movie::where('is_active', true)->take(5)->get(); 
        return view('pages.index', compact('movies'));
    }

    public function movieDetail($id){
        $movie_detail = Movie::findorFail($id);
        return view('pages.movie_single',compact('movie_detail'));
    }
    

}
