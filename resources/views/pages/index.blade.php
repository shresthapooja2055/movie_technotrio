@extends('master')
@section('content')

<!-- Features Jobs -->
<div class="section gray margin-top-45 padding-top-65 padding-bottom-75">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				
				<!-- Section Headline -->
				<div class="section-headline margin-top-0 margin-bottom-35">
					<h3>Featured Movies</h3>
					{{-- <a href="jobs-list-layout-full-page-map.html" class="headline-link">Browse All Movies</a> --}}
				</div>
				
				<!-- Jobs Container -->
				<div class="listings-container compact-list-layout margin-top-35">
				 @foreach($movies as $movie)	
					<!-- Job Listing -->
					<a href="{{ route('movie_detail', $movie->id)}}" class="job-listing with-apply-button">

						<!-- Job Listing Details -->
						<div class="job-listing-details">

							<!-- Logo -->
							<div class="job-listing-company-logo">
								<img src="images/company-logo-01.png" alt="">
							</div>

							<!-- Details -->
							<div class="job-listing-description">
								<h3 class="job-listing-title">{{ ucwords($movie->title) }}</h3>
								<!-- Job Listing Footer -->
								<div class="job-listing-footer">
									<ul>
										{{-- <li><i class="icon-material-outline-business-center"></i>{{ $movie->movie_category->name }}</li> --}}
										<li><i class="icon-material-outline-access-time"></i>{{date('d M Y', strtotime($movie->release_date))}}</li>


                                        <?php
                                                $totalLikes =DB::table('favourite_movie')
                                                ->join('movies','movies.id','favourite_movie.movie_id')
                                                ->where('favourite_movie.movie_id', $movie->id)
                                                ->get()->count();
                                        ?>

                                        <li>No. of likes ( {{ $totalLikes }} )</li>

									</ul>
                                    <ul>
										<li>{!! Str::limit($movie->description,30) !!}</li>
                                        <li><img src="{{asset('uploads/'.$movie->featured_image)}}"  alt="image" width="150" height="100" /></li>
									</ul>
								</div>
							</div>

							<!-- Apply Button -->
                            @guest
                            <a href="{{ route('login') }}" class="apply-now-button">Add to Favourite <i
                                class="icon-material-outline-arrow-right-alt"></i></a>
                            @endguest

                            @auth
                                <button type="button" class="btn btn-primary"  onclick="favMovie({{ $movie->id }})" style="height: 40px;">Add to Favourite
                                </button>
                            @endauth

						</div>
					</a>	
                 @endforeach

				</div>
				<!-- Jobs Container / End -->

			</div>
		</div>
	</div>
</div>
<!-- Featured Jobs / End -->

<script>
    function favMovie(id){
        // console.log(id)
        $.ajax({
                url: "{{ route('favourite_movie_store') }}",
                type: "POST",
                data: {
                    id: id,
                    _token: "{{ csrf_token() }}"
                },
                success: function(data) {
                    console.log(data);
                    if (data.status == 0) {
                        toastr.error(data.error);
                    } else {
                        toastr.success(data.msg);
                        // location.reload();
                    }
                }
            });
    }

</script>   
@endsection
