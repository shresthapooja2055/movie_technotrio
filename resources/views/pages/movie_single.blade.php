@extends('master')
@section('content')
    <!-- Titlebar
                        ================================================== -->
    <div class="single-page-header" data-background-image="{{ asset('assets/images/single-job.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="single-page-header-inner">
                        <div class="left-side">
                            <div class="header-image"><a href="#"><img
                                        src="{{ asset('assets/images/company-logo-05.png') }}" alt=""></a></div>
                            <div class="header-details">
                                <h3>{{ $movie_detail->title }}</h3>
                                <ul>


                                    {{-- <li><a href="#"><i
                                                class="icon-material-outline-business"></i>{{ $movie_detail->movie_category->name }}
                                        </a></li> --}}

                                    <li><i class="icon-material-outline-location-on"></i>{{ $movie_detail->release_date }}
                                    </li>

                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Page Content
                        ================================================== -->
    <div class="container">
        <div class="row">

            <!-- Content -->
            <div class="col-xl-8 col-lg-8 content-right-offset">
                <div class="single-page-section">
                    <h3 class="margin-bottom-25">Movie Description</h3>
                    <p>{!! $movie_detail->description !!}</p>
                </div>

                <div class="single-page-section">
                    <h3 class="margin-bottom-30">Poster</h3>
                    <div id="single-job-map-container">
                        <img src="{{ asset('uploads/' . $movie_detail->featured_image) }}" alt="image" width="450"
                            height="250" />
                    </div>
                </div>
            </div>

            <!-- Sidebar -->
            <div class="col-xl-4 col-lg-4">
                <div class="sidebar-container">

                    @auth
                        @if (auth()->user()->role_as == '0')
                            <button type="button" class="btn btn-primary w-100 mb-3"
                                onclick="favMovie({{ $movie_detail->id }})" style="height: 40px;">Add to Favourite
                            </button>
                        @else
                            <a href="{{ route('index') }}" class="btn btn-primary w-100 mb-3"> View All Movies
                                <i class="icon-material-outline-arrow-right-alt"></i> </a>
                        @endif
                    @endauth

                    @guest
                        <a href="{{ route('login') }}" class="btn btn-primary w-100 mb-3">Add To Favourite<i
                                class="icon-material-outline-arrow-right-alt"></i></a>
                    @endguest
                    <!-- Sidebar Widget -->
                    <div class="sidebar-widget">
                        <div class="job-overview">
                            <div class="job-overview-headline">Movie Summary</div>
                            <div class="job-overview-inner">
                                <ul>
                                    <li>
                                        <i class="icon-material-outline-location-on"></i>
                                        <span>Title</span>
                                        <h5>{{ $movie_detail->title }}</h5>
                                    </li>
                                    <li>
                                        <i class="icon-material-outline-business-center"></i>
                                        <span>Movie Category</span>
                                        {{-- <h5>{{ $movie_detail->movie_category->name }}</h5> --}}
                                    </li>
                                    <li>

                                        <?php
                                        $totalLikes = DB::table('favourite_movie')
                                            ->join('movies', 'movies.id', 'favourite_movie.movie_id')
                                            ->where('favourite_movie.movie_id', $movie_detail->id)
                                            ->get()
                                            ->count();
                                        ?>
                                        <i class="icon-material-outline-local-atm"></i>
                                        <span>No. of likes</span>
                                        <h5>{{ $totalLikes}}</h5>
                                    </li>
                                    <li>
                                        <i class="icon-material-outline-access-time"></i>
                                        <span> Release Date</span>
                                        <h5> {{ $movie_detail->release_date }}</h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <script>
                function favMovie(id) {
                    // console.log(id)
                    $.ajax({
                        url: "{{ route('favourite_movie_store') }}",
                        type: "POST",
                        data: {
                            id: id,
                            _token: "{{ csrf_token() }}"
                        },
                        success: function(data) {
                            console.log(data);
                            if (data.status == 0) {
                                toastr.error(data.error);
                            } else {
                                toastr.success(data.msg);
                                // location.reload();
                            }
                        }
                    });
                }
            </script>
        @endsection
