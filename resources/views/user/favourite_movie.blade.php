@extends('dashboard.master')
@section('content')
    <!-- Dashboard Container -->
    <div class="dashboard-container">

        <!-- Dashboard Sidebar
                             ================================================== -->
        @include ('dashboard.user_side_nav')

        <!-- Dashboard Sidebar / End -->

        <!-- Dashboard Content
                             ================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner">
            
                <!-- Dashboard Headline -->
                @if ($total > 0)
                    <div class="dashboard-headline">
                        <h3> Favourite Movies </h3>
                        <!-- Breadcrumbs -->
                        <nav id="breadcrumbs" class="dark">
                            <ul>
                                <li><a href="{{ route('favourite_movie') }}">Favourite Movies</a></li>
                                <li><a href="{{ route('index') }}">Browse Movie</a></li>
                            </ul>
                        </nav>
                    </div>
                  
                    <!-- Row -->
                    <div class="row">

                        <!-- Dashboard Box -->
                        <div class="col-xl-12">
                            <div class="dashboard-box margin-top-0">
    
                                <!-- Headline -->
                                <div class="headline">
                                    <h3><i class="icon-material-outline-business-center"></i> Favourite Movie</h3>
    
                                    <div class="container">
                                        <table class="table table-bordered mt-5">
                                            <thead>
                                                <tr>
                                                    <th>S.N</th>
                                                    <th>Title</th>
                                                    <th>Description</th>
                                                    <th>Release Date</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($favourite_movies as $key => $movie)
                                                    <tr>
                                                        <td>{{ $key + 1 }}</td>
                                                        <td><a href="{{ route('movie_detail', $movie->id) }}">{{ $movie->title }}</a></td>
                                                        <th>{!! $movie->description !!}</th>
                                                        <td>{{ $movie->release_date }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
    
    
                            </div>
                        </div>
    
                    </div>
                @else
                    <div class="row">
                        <!-- Dashboard Box -->
                        <div class="col-xl-12">
                            <div class="dashboard-box margin-top-0">
                                <!-- Headline -->
                                <div class="headline">
                                    <h3></h3>
                                    <i class="icon-material-outline-note-add">  </i><a
                                        href="{{ route('index') }}">Add Movie To Favourite</a></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <!-- Row / End -->
            @endsection
