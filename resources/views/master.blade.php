<!doctype html>
<html lang="en">
<head>
    @include('includes.head')
</head>
<body>

  <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v14.0" nonce="xkcR4nU6"></script>

    <div id="wrapper">

      <div class="clearfix"></div>
   
      @include('includes.header')
      <!-- main content -->
      @yield('content')
      <!-- footer -->
      @include('includes.footer')

    </div>
    
</body>
</html>
