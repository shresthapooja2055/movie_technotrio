@extends('dashboard.master')
@section('content')
    <style>
        .pointer {
            cursor: pointer;
        }
    </style>

    <!-- Dashboard Container -->
    <div class="dashboard-container">

        <!-- Dashboard Sidebar
         ================================================== -->
        @include('dashboard.admin_side_nav')

        <!-- Dashboard Sidebar / End -->

        <!-- Dashboard Content
         ================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner">

                <!-- Dashboard Headline -->
                <div class="dashboard-headline">
                    <h3>Movie List</h3>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                            <li><a href="{{ route('movie_add') }}">Post Movie</a></li>
                        </ul>
                    </nav>
                </div>

                @if (Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
                <!-- Row -->
                <div class="row">

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-business-center"></i> Movie Listings</h3>

                                <div class="container">
                                    <table class="table table-bordered mt-5">
                                        <thead>
                                            <tr>
                                                <th>S.N</th>
                                                <th>Name</th>
                                                <th>Description</th>
                                                {{-- <th>Category</th> --}}
                                                <th>Release Date</th>
                                                <th>Poster</th>
                                                <th>Status</th>
                                                <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($movies as $key => $movie)
                                            
                                                <tr>
                                                    <td>{{ $key + 1 }}</td>
                                                    <td><a href="{{ route('movie_detail', $movie->id)}}">{{ $movie->title }}</a></td>
                                                    <td>{!! $movie->description !!}</td>
                                                    {{-- <td>{{ $movie->movie_category->name }}</td> --}}
                                                    <td>{{ $movie->release_date }}</td>
                                                    <td><img src="{{asset('uploads/'.$movie->featured_image)}}"  alt="image" width="150" height="80" /></td>


                                                    <td>
                                                        <form action="{{ route('change_status') }}" method="post">
                                                            @csrf
                                                                <div class="form-check form-switch">
                                                                 <input type="hidden" name="id" value="{{ $movie->id }}">
                                                                 <input class="form-check-input" type="checkbox" name="is_active" onchange="this.form.submit()" role="switch"  {{ $movie->is_active ? 'checked' : '' }}>
                                                                </div>
                                                        </form>
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('movie_edit', $movie->id) }}"
                                                            class="button gray ripple-effect ico" title="Edit"
                                                            data-tippy-placement="top"><i class="icon-feather-edit"></i></a>
                                                        <a href="{{ route('movie.delete', $movie->id) }}"
                                                            class="button gray ripple-effect ico" title="Remove"
                                                            data-tippy-placement="top"><i
                                                                class="icon-feather-trash-2"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row / End -->
                
            @endsection
