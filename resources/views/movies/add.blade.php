@extends('dashboard.master')

@section('content')
    <!-- Dashboard Container -->
    <div class="dashboard-container">

        <!-- Dashboard Sidebar
         ================================================== -->
        @include('dashboard.admin_side_nav')

        <!-- Dashboard Sidebar / End -->


        <!-- Dashboard Content
         ================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner">

                <!-- Dashboard Headline -->
                <div class="dashboard-headline">
                    <h3>Post a Movie</h3>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('index') }}">Home</a></li>
                            <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                            <li><a href="{{ route('movie_list') }}">View Movie</a></li>
                        </ul>
                    </nav>
                </div>

                @if (Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
                <!-- Row -->
                <div class="row">

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-feather-folder-plus"></i> Movie Submission Form</h3>
                            </div>

                            <div class="content with-padding padding-bottom-10">
                                <form action="{{ route('movie_store') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">

                                        <div class="col-xl-6">
                                            <div class="submit-field">
                                                <h5>Movie Title</h5>
                                                <input type="text" class="with-border" name="title"
                                                    placeholder="Movie Title" value="{{ old('title') }}">
                                                {!! $errors->first('title', '<small class="text-danger">:message </small>') !!}
                                            </div>
                                        </div>


                                        <div class="col-xl-6">
                                            <div class="submit-field">
                                                <h5>Movie Category</h5>
                                                <select class="selectpicker with-border" data-size="7"
                                                    title="Select Category" name="movie_category_id">
                                                    @foreach ($movieCategory as $category)
                                                        <option value="{{ $category->id }}">{{ $category->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                {!! $errors->first('movie_category_id', '<small class="text-danger">:message </small>') !!}
                                            </div>
                                        </div>

                                        
                                        <div class="col-xl-6">
                                            <div class="submit-field">
                                                <h5>Release Date</h5>

                                                <div class="input-with-icon">
                                                    <input class="with-border" type="date" placeholder="Release Date"
                                                        name="release_date">
                                                </div>
                                                {!! $errors->first('release_date', '<small class="text-danger">:message </small>') !!}

                                            </div>
                                        </div>

                                        <div class="col-xl-6">
                                            <div class="submit-field">
                                                <h5>Status</h5>
                                                <select class="selectpicker with-border" data-size="7" title="Status"
                                                    name="is_active">
                                                    <option value="1">Publish</option>
                                                    <option value="0">Unpublish</option>
                                                </select>
                                                {!! $errors->first('is_active', '<small class="text-danger">:message </small>') !!}

                                            </div>
                                        </div>

                                        <div class="col-xl-12">
                                            <div class="submit-field">
                                                <h5>Upload A Poster</h5>
                                                <div class="input-with-icon">
                                                    <input class="with-border" type="file" name="featured_image">
                                                </div>
                                                {!! $errors->first('featured_image', '<small class="text-danger">:message </small>') !!}

                                            </div>
                                        </div>

                                        <div class="col-xl-12">
                                            <div class="submit-field">
                                                <h5>Movie Description</h5>
                                                <textarea cols="30" rows="5" id="movie_description" class="with-border" name="description">{{ old('description') }}</textarea>
                                                {!! $errors->first('description', '<small class="text-danger">:message </small>') !!}
                                            </div>
                                        </div>

                                        <div class="col-xl-12">
                                            <button type="submit" class="button ripple-effect big margin-top-30"><i
                                                    class="icon-feather-plus"></i> Post a Movie</button>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row / End -->
                <script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script>
                    ClassicEditor
                        .create(document.querySelector('#movie_description'))
                        .catch(error => {
                            console.error(error);
                        });
                </script>
            @endsection
