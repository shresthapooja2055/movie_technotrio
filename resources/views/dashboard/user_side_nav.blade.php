<div class="dashboard-sidebar">
    <div class="dashboard-sidebar-inner" data-simplebar>
        <div class="dashboard-nav-container">

            <!-- Responsive Navigation Trigger -->
            <a href="#" class="dashboard-responsive-nav-trigger">
                <span class="hamburger hamburger--collapse">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </span>
                <span class="trigger-title">Dashboard Navigation</span>
            </a>

            <!-- Navigation -->
            <div class="dashboard-nav">
                <div class="dashboard-nav-inner">

                    <ul data-submenu-title="Start">
                        <li><a href="{{ route('user_profile')}}"><i class="icon-material-outline-dashboard"></i>
                                Profile</a></li>

                        <li><a href="{{ route('favourite_movie') }}"><i class="icon-material-outline-dashboard"></i>
                                Favourite Movie</a></li>
                    </ul>
                </div>
            </div>
            <!-- Navigation / End -->

        </div>
    </div>
</div>
