<div class="dashboard-sidebar">
    <div class="dashboard-sidebar-inner" data-simplebar>
        <div class="dashboard-nav-container">

            <!-- Responsive Navigation Trigger -->
            <a href="#" class="dashboard-responsive-nav-trigger">
                <span class="hamburger hamburger--collapse">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </span>
                <span class="trigger-title">Dashboard Navigation</span>
            </a>

            <!-- Navigation -->
            <div class="dashboard-nav">
                <div class="dashboard-nav-inner">

                    <ul data-submenu-title="Start">
                        <li class="active"><a href="{{ route('dashboard') }}"><i
                                    class="icon-material-outline-dashboard"></i> Dashboard</a></li>

                        <li><a href="#"><i
                                    class="icon-material-outline-rate-review"></i> Profile</a></li>
                    </ul>

                    <ul data-submenu-title="Movie Management">
                        <li><a href="#"><i class="icon-material-outline-business-center"></i> Movies</a>
                            <ul>
                                <li><a href="{{ route('movie_list') }}">Movie List</a></li>
                                <li><a href="{{ route('movie_add') }}"> Post Movie</a></li> 
                            </ul>
                        </li>
                    </ul>

                    <ul data-submenu-title="User Management">
                        <li><a href="{{ route('user_list') }}"> <i class="icon-material-outline-business-center"></i>User List</a>
                           
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Navigation / End -->

        </div>
    </div>
</div>
