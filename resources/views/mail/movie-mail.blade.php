@component('mail::message')
# Hello, {{ $user }}

"{{ $movie->title }}", has been added to your favourite list.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
