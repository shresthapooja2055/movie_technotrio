@extends('dashboard.master')
@section('content')
    <style>
        .pointer {
            cursor: pointer;
        }
    </style>

    <!-- Dashboard Container -->
    <div class="dashboard-container">

        <!-- Dashboard Sidebar
                     ================================================== -->
        @include('dashboard.admin_side_nav')

        <!-- Dashboard Sidebar / End -->


        <!-- Dashboard Content
                     ================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner">

                <!-- Dashboard Headline -->
                <div class="dashboard-headline">
                    <h3>User List</h3>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                            <li><a href="{{ route('movie_add') }}">Post Movie</a></li>
                        </ul>
                    </nav>
                </div>


                <!-- Row -->
                <div class="row">
                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">
                            <div class="content with-padding padding-bottom-10">
                                <form action="{{ route('movie.search') }}" method="get">
                                    <div class="row">

                                        <div class="col-xl-5">
                                            <div class="submit-field">
                                              <label for="datepicker">From</label>
                                              <input placeholder="Select your date" type="date" name="fromDate"
                                                  id="datepicker" value="">
                                            </div>     
                                          </div>

                                          <div class="col-xl-5">
                                            <div class="submit-field">
                                              <label for="datepicker">To</label>
                                              <input placeholder="Select your date" type="date" name="toDate"
                                                  id="datepicker" value="">
                                            </div>      
                                          </div>

                                          <div class="col-xl-2">
                                            <div class="submit-field">
                                                <button type="submit" class="btn btb-primary mt-4">Search</button>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>


                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-business-center"></i> User Listings</h3>                       

                                <div class="container">
                                    <table class="table table-bordered mt-5">
                                        <thead>
                                            <tr>
                                                <th>S.N</th>
                                                <th>Name</th>
                                                <th>Favourite Movie</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($users as $key => $user)
                                                <?php
                                                
                                                $total = DB::table('favourite_movie')
                                                    ->join('movies', 'movies.id', 'favourite_movie.movie_id')
                                                    ->join('users', 'users.id', 'favourite_movie.user_id')
                                                    ->where('favourite_movie.user_id', $user->id)
                                                    ->get();
                                                ?>

                                                <tr>
                                                    <td>{{ $key + 1 }}</td>
                                                    <td>{{ $user->name }} </td>
                                                    <td>
                                                        @if ($total->count() > 0)
                                                            ({{ $total->count() }})
                                                            <a href="{{ route('fav_movie_details', $user->id) }}"
                                                                style="margin-left:20px;"> View</a>
                                                        @else
                                                            0
                                                        @endif
                                                    </td>

                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
                <!-- Row / End -->
            @endsection
