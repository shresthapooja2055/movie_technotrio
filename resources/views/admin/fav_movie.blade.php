@extends('dashboard.master')
@section('content')
    <style>
        .pointer {
            cursor: pointer;
        }
    </style>

    <!-- Dashboard Container -->
    <div class="dashboard-container">

        <!-- Dashboard Sidebar
                 ================================================== -->
        @include('dashboard.admin_side_nav')

        <!-- Dashboard Sidebar / End -->


        <!-- Dashboard Content
                 ================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner">

                <!-- Dashboard Headline -->
                <div class="dashboard-headline">
                    {{-- <h3>Vacancy List</h3> --}}

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('index') }}">View Movie</a></li>
                            <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                            <li><a href="{{ route('movie_add') }}">Post Movie</a></li>
                        </ul>
                    </nav>
                </div>

                <!-- Row -->
                <div class="row">

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">
                            <div class="content with-padding padding-bottom-10">
                                <form action="{{ route('movie.search') }}" method="get">
                                    <div class="row">

                                        <div class="col-xl-5">
                                            <div class="submit-field">
                                                <label for="datepicker">From</label>
                                                <input placeholder="Select your date" type="date" name="fromDate"
                                                    id="datepicker" value="">
                                            </div>
                                        </div>

                                        <div class="col-xl-5">
                                            <div class="submit-field">
                                                <label for="datepicker">To</label>
                                                <input placeholder="Select your date" type="date" name="toDate"
                                                    id="datepicker" value="">
                                            </div>
                                        </div>

                                        <div class="col-xl-2">
                                            <div class="submit-field">
                                                <button type="submit" class="btn btb-primary mt-4">Search</button>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-business-center"></i> Favourite Movie</h3>

                                @if ($fav_movies->count() > 0)
                                    <div class="container">
                                        <table class="table table-bordered mt-5">
                                            <thead>
                                                <tr>
                                                    <th>S.N</th>
                                                    <th>Name</th>
                                                    <th>Release Date</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($fav_movies as $key => $movie)
                                                    <tr>
                                                        <td>{{ $key + 1 }}</td>
                                                        <td><a href="{{ route('movie_detail', $movie->movie_id) }}">{{ $movie->title }}</a></td>
                                                        <td>{{ $movie->release_date }} </td>

                                                    </tr>
                                                @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                @else
                                    <div class="container">
                                        <div class="row mb-5">
                                            <h2 style="text-align:center"> Sorry , No  favourite movies available. </h2>
                                        </div>
                                    </div>
                                @endif


                            </div>


                        </div>
                    </div>

                </div>
                <!-- Row / End -->


            @endsection
