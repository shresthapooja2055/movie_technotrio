@extends('dashboard.master')
@section('content')

        <!-- Dashboard Container -->
        <div class="dashboard-container">

            <!-- Dashboard Sidebar
 ================================================== -->
          @include('dashboard.admin_side_nav')
            <!-- Dashboard Sidebar / End -->


            <!-- Dashboard Content
 ================================================== -->
            <div class="dashboard-content-container" data-simplebar>
                <div class="dashboard-content-inner">

                    <!-- Dashboard Headline -->
                    <div class="dashboard-headline">
                        <h3>{{ auth()->user()->name }}</h3>
                        <span>We are glad to see you again!</span>

                        <!-- Breadcrumbs -->
                        <nav id="breadcrumbs" class="dark">
                            <ul>
                                <li><a href="{{ route('index')}}">Home</a></li> 
                                <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
                            </ul>
                        </nav>
                    </div>
                 
@endsection                    

                
