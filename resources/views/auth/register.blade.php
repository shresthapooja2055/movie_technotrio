@extends('master')

@section('content')

<div id="titlebar" class="gradient">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="{{ route('index') }}">Home</a></li>
                        <li><a href="{{ route('login') }}">Login</li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-xl-5 offset-xl-3">
            <div class="login-register-page">
                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3 style="font-size: 26px;">Register</h3>
                    <span>Already have an account?<a href="{{ route('login') }}">Log In!</a></span>
                </div>

                <!--  employer Form -->
                <form method="post" action="{{ route('register') }}">
                    @csrf

                    @if (Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                        @php
                        Session::forget('success');
                        @endphp
                    </div>
                    @endif

                    <div class="input-with-icon-left">
                        <i class="icon-material-baseline-mail-outline"></i>
                        <input type="text" class="input-text with-border" name="name" id="name" value="{{ old('name')}}" placeholder="Full Name">
                        {!! $errors->first('name', '<small class="text-danger">:message </small>') !!}
                    </div>

                    <div class="input-with-icon-left">
                        <i class="icon-material-baseline-mail-outline"></i>
                        <input type="text" class="input-text with-border" name="email" id="email" value="{{ old('email')}}"
                        placeholder="Official Email">
                        {!! $errors->first('email', '<small class="text-danger">:message</small>') !!}
                    </div>

                    <div class="input-with-icon-left" title="Should be at least 8 characters long" data-tippy-placement="bottom">
                        <i class="icon-material-outline-lock"></i>
                        <input type="password" class="input-text with-border" name="password" id="password"
                        placeholder="Password">
                        {!! $errors->first('password', '<small class="text-danger">:message</small>') !!}
                    </div>

                    <div class="input-with-icon-left">
                        <i class="icon-material-outline-lock"></i>
                        <input type="password" class="input-text with-border" name="password_confirmation"
                        id="password_confirmation" placeholder="Confirm Password">
                        {!! $errors->first('password_confirmation', '<small class="text-danger">:message</small>') !!}
                    </div>

                    <div class="form-group mb-6 captcha">
                        <div class="g-recaptcha" data-sitekey="6LczpD4gAAAAAG9dgLKK76bQMjscIf3aE3lhdgDj"></div>
                        {!! $errors->first('g-recaptcha-response', '<small class="text-danger">:message</small>') !!}
                    </div>

                    <!-- Button -->
                    <button class="button full-width button-sliding-icon ripple-effect margin-top-10" type="submit">Register <i class="icon-material-outline-arrow-right-alt"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Spacer -->
<div class="margin-top-70"></div>
<!-- Spacer / End-->

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

@endsection
