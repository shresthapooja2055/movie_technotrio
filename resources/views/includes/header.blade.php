<header id="header-container" class="fullwidth">
    <!-- Header -->
    <div id="header">
        <div class="container">
            <!-- Left Side Content -->
            <div class="left-side">

                <!-- Logo -->
                <div id="logo">
                </div>

                <!-- Main Navigation -->
                <nav id="navigation">
                    <ul id="responsive">

                        <li><a href="{{ route('index') }}" class="current abc">Home</a></li>

                        {{-- <li><a href="{{ route('job_list') }}" class="abc">Vacancies</a></li> --}}

                        @guest
                            <li><a href="#" class="new-menu"><i class="icon-line-awesome-user"></i></a>
                                <ul class="dropdown-nav">
                                    <li><a href="{{ route('login') }}">Login</a></li>
                                    <li><a href="{{ route('register') }}">Register</a></li>
                                </ul>
                            </li>
                        @endguest


                        @auth
                            <li><a href="#" class="new-menu">{{ auth()->user()->name }}</a>
                                <ul class="dropdown-nav">
                                    @if (auth()->user()->role_as == '1')
                                        <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                    @else
                                        <li><a href="{{ route('user_profile')}}">Profile</a></li>
                                    @endif
                                    
                                    <form method="POST" action="{{ route('logout') }}">
                                        @csrf
                                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                        this.closest('form').submit();">Logout</a></li>
                                  </form>

                                </ul>
                            </li>
                        @endauth
                    </ul>
                </nav>
                <div class="clearfix"></div>
                <!-- Main Navigation / End -->
            </div>
            <!-- Left Side Content / End -->
        </div>
    </div>
    <!-- Header / End -->

</header>
