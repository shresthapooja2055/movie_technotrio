<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Home\HomeController;
use App\Http\Controllers\Movie\MovieController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// home
Route::get('/', [HomeController::class,'index'])->name('index');

Route::get('/movie-detail/{id}', [HomeController::class,'movieDetail'])->name('movie_detail');



Route::middleware(['auth','isAdmin'])->group(function(){

Route::get('/dashboard', function () {
    return view('admin.dashboard');
})->name('dashboard');

// movie
Route::get('/movie/list', [MovieController::class, 'movieIndex'])->name('movie_list');
Route::get('/movie/add', [MovieController::class, 'movieAdd'])->name('movie_add');
Route::post('/movie/add/store', [MovieController::class, 'movieStore'])->name('movie_store');
Route::get('/movie/edit/{id}', [MovieController::class, 'movieEdit'])->name('movie_edit');
Route::post('/movie/edit/update/{id}', [MovieController::class, 'movieUpdate'])->name('movie_update');
Route::get('movie/delete/{id}', [MovieController::class, 'destroy'])->name('movie.delete');
Route::post('/changeStatus', [MovieController::class, 'changeStatus'])->name('change_status');

// user list
Route::get('/user-list', [MovieController::class, 'userList'])->name('user_list');

// filter
Route::get('/search', [MovieController::class, 'search'])->name('movie.search');

});

// view user's favourite movie
Route::get('/favourite-movie-details/{id}', [MovieController::class, 'favMovie'])->name('fav_movie_details');

require __DIR__.'/auth.php';



// User Profile 
Route::get('/user-profile', [MovieController::class,'userProfile'])->name('user_profile');

// User Favourite Movie
Route::post('/favourite-movie-form/store', [MovieController::class,'movieSubmit'])->name('favourite_movie_store');
Route::get('/user/favourite-movie', [MovieController::class,'favouriteMovie'])->name('favourite_movie');

